.data
    i:  .word 0     # Main register R1
    j:  .word 0     # Main register R3
    x:  .word 0     # Main register R5

.text
main:
    daddi   R4, R0, 10
    
Loop:
    lw      R1, i(R0)       # load i
    lw      R2, j(R0)       # load j
    lw      R3, x(R0)       # load x
    daddi   R1, R1, 1       # i = i + 1
    daddi   R2, R1, -1      # j = i - 1
    dmul    R5, R1, R2      # aux = i * j
    daddi   R1, R1, 1       # i++
    sw      R1, i(R0)       # store i
    sw      R2, j(R0)       # store j
    dadd    R3, R3, R5      # x = x + aux
    sw      R3, x(R0)       # store x
  
    bne     R1 ,R4, Loop    # branch if(r1 != 10)

Finish: 

    halt